<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    
    public function pendataan()
    {
        return view ('halaman.pendataan');
    }
 
    public function welcome(Request $request)
    {
        //dd($request->all());
        $name = $request['name'];
        $lastname = $request['lastname'];
        return view ('halaman.welcome', compact('name','lastname'));
    }
} 
