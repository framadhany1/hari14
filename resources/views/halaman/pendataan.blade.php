@extends('layout.master')
@section('judul')
     Halaman Pendataan
@endsection

@section('content')
    

<form action="/kirim" method="post">
    @csrf
    <label> Frist Name :</label><br>
    <input type="text" name="name"><br><br>
    <label> Last Name :</label><br>
    <input type="text" name="lastname"><br><br>
    
    <label>Gender :</label> <br>
    <input type="radio" id="male" name="gender" value="30">
    <label for="male"> Male </label><br>
    <input type="radio" id="female" name="gender" value="30">
    <label for="female"> Female </label><br><br>
  
    <label> Nationality</label><br><br>
    <select name="nationality" >
            <option value="indonesia"> Indonesia</option>
            <option value="inggris"> Inggris</option>
            <option value="amerika"> Amerika</option>
    </select><br><br>

    <label>Language Spoken</label><br>
    <input type="checkbox" name="language"> Bahasa Indonesia<br>
    <input type="checkbox" name="language"> Inggris<br>
    <input type="checkbox" name="language"> Other<br>

    <label>Bio</label><br>
    <textarea name="message" rows="15" cols="40"></textarea>
    <br><br>

    <button>Sign Up</button>

</form>

@endsection